import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import dotenv from 'dotenv'; 
dotenv.config()

const PK_META: string = process.env.PK_META ? process.env.PK_META : '' 

const config: HardhatUserConfig = {
  solidity: "0.8.19",
  networks: {
    hardhat: {
    },
    // fb_optimism: {
    //   url: `http://127.0.0.1:${process.env.PORT}/${process.env.FB_API_KEY}`,
    //   // accounts: [process.env.PK_META, process.env.ETHER_TATE],
    //   gasPrice: 500000000
    // },
    optimism: {
      url: process.env.QUICK_NODE_OPTIMISM,
      accounts: [PK_META],
      gasPrice: 50000000,
    }
  }
};

export default config;
