import dotenv from 'dotenv'; 
dotenv.config()

const serverURL: string = process.env.EULITH_URL ?? "https://opt-main.eulithrpc.com/v0";

const token: string = process.env.EULITH_TOKEN ? process.env.EULITH_TOKEN : "";

const Wallet1: string = process.env.PK_META ? `0x${process.env.PK_META}` : '';
// const Wallet2: string = "0xddeff2733e6142c873df7bede7db29055471ebeae7090ef618996a51daa4cd8c";

export default {
    serverURL,
    token,
    Wallet1,
    // Wallet2
};