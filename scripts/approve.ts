import { ethers } from "hardhat";

import * as Eulith from "eulith-web3js";

import config from "./commonConfiguration";

const eulithAuth = Eulith.Auth.fromToken(config.token);
const provider = new Eulith.Provider({
    network: Eulith.Networks.Predefined.mainnet.with({ eulithURL: config.serverURL }),
    auth: eulithAuth,
});

const acct = new Eulith.Signing.LocalSigner({ privateKey: config.Wallet1 });
const signer = Eulith.Signing.SigningService.assure(acct, provider);

const OP = "0x4200000000000000000000000000000000000042"
const UNI_ROUTER = "0x68b3465833fb72a70ecdf485e0e4c7bd8665fc45"
const WETH = "0x4200000000000000000000000000000000000006"

async function main() {
  console.log(acct.address)

  const armorAgent = await Eulith.OnChainAgents.getArmorAgent({
    provider: provider,
    tradingKeyAddress: acct.address,
    tradingKeySigner: signer
  });
  console.log(armorAgent.safeAddress)


  const armor_provider = new Eulith.Provider({
    network: Eulith.Networks.Predefined.mainnet.with({ eulithURL: config.serverURL }),
    auth: eulithAuth,
    safeAddress: armorAgent.safeAddress,
    signer: signer,
  })

  var tx = {
    from: armorAgent.safeAddress,
    to: armorAgent.safeAddress,
    value: "10",
    gas: "500000",
    gasPrice: "50000000",
    data: "",
    nonce: 574,
    chainId: 10, 
  }

  let res = await armor_provider.signAndSendTransaction(tx, signer)
  console.log(res)
  // const op = await ethers.getContractAt("IERC20", OP)
  // console.log(await op.balanceOf(armorAgent.safeAddress))

  // let tx = await op.approve(UNI_ROUTER, 1)
  // let receipt = await tx.wait(1)
  // console.log(receipt)

  // const atomicTx = new Eulith.AtomicTx.Transaction({ provider, signer: signer });

  // const swapAtomicTx: Eulith.AtomicTx.NestedTransaction = await Eulith.Uniswap.startSwap({
  //     request: quote.swapRequest,
  //     parentTx: atomicTx
  // });
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
