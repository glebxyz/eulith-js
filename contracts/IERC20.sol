interface IERC20 {
    function transfer(address, uint256) external returns (uint256);

    function transferFrom(address, address, uint256) external returns (uint256);

    function approve(address, uint256) external returns (uint256);

    function allowance(address, address) external view returns (uint256);

    function balanceOf(address) external view returns (uint256);
}
